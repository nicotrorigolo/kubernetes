#!/bin/bash



nom_cluster=legume

echo "Démarrage de l'installation du cluster k3d"
echo "-------------------------------------------------------------------"

echo "Recuperer le paquet d'installation de k3d"
curl -s https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | TAG=v5.0.0 bash

echo "Creation du cluster"
k3d cluster create $nom_cluster

echo "Choisir de se connecter a un cluster"
export KUBECONFIG=kubeconfig-$nom_cluster.yml

echo "Afficher la composition du cluster"
k3d kubeconfig write $nom_cluster

echo "Deplacer le fichier de composition du cluster a la racine de l'utilisateur"
cp /home/vagrant/.k3d/kubeconfig-$nom_cluster.yaml /home/vagrant/kubeconfig-$nom_cluster.yml

echo "-------------------------------------------------------------------"
echo "Fin du lancement du fichier initialisation_cluster_k3d.sh"
echo "-------------------------------------------------------------------"









