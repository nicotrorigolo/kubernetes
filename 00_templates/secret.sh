kubectl create secret docker-registry {nom_du_secret} \
    --docker-server={url_registry} \   #Exemple : registry.gitlab.com
    --docker-username={username_pour_registry}  \
    --docker-password={mot_de_passe_pour_registry}  \
    --docker-email={email_pour_registry}
