# Création d'un volume pour stocker l'image de l'OS
resource "libvirt_volume" "ubuntu_qcow2" {
  name   = "ubuntu_${var.name}_qcow2"
  pool   = "${var.pool}"
  source = "https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64.img"
  format = "qcow2"
}

resource "libvirt_volume" "test-os_image" {
  name   = "ubuntu_resize_${var.name}_qcow2"
  base_volume_id  = libvirt_volume.ubuntu_qcow2.id
  pool            = "${var.pool}"
  size            = 10737418240
}
