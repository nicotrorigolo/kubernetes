#!/bin/bash

export DEBIAN_FRONTEND='noninteractive'

echo "Mise à jour ubuntu"
apt -y update \
&& apt -y upgrade

echo "Passer le clavier en azerty"
sudo loadkeys fr

# echo "Installation de minikube"
# curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube \
# && mkdir -p /usr/local/bin/ \
# && install minikube /usr/local/bin/

echo "Installation de docker"
apt install -y docker.io \
&& usermod -aG docker vagrant

echo "Configuration de apt pour kubernetes"
apt-get update && apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
add-apt-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"

echo "Installation de kubernetes"
apt-get install -y kubelet kubeadm kubectl kubernetes-cni
systemctl enable kubelet

echo "Ajout de l'autocompletion de kubernetes"
source <(kubectl completion bash)
echo "source <(kubectl completion bash)" >> ~/.bashrc

echo "Pour la maintenance"
apt install -y net-tools

echo "-------------------------------------------------------------------"
echo "Fin des installation du fichier master_requirements.sh"
echo "-------------------------------------------------------------------"